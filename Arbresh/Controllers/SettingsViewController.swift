//
//  SettingsViewController.swift
//  Arbresh
//
//  Created by Armend B on 16.01.18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var tableView: UITableView!
    
    private var sectionOne: Array<String>!
    private var sectionTwo: Array<String>!
    private var sectionThree: Array<String>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor

        self.title = "Preferencat"
        
        self.setupSections()
        self.setupControls()
    }

    func setupControls() {
        
        // TableView
        tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
    }
    
    override func viewWillLayoutSubviews() {
        
        // TableView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.origin.y = 0.0
            rect.size.width = self.view.frame.width
            rect.size.height = self.view.frame.height
            
            tableView.frame = rect
        }
    }
    
    //MARK - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return sectionOne.count
        } else if section == 1 {
            return sectionTwo.count
        } else if section == 2 {
            return sectionThree.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        if indexPath.section == 0 {
            cell?.textLabel?.text = sectionOne [indexPath.row]
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        } else if indexPath.section == 1 {
            cell?.textLabel?.text = sectionTwo [indexPath.row]
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        } else if indexPath.section == 2 {
            cell?.textLabel?.text = sectionThree [indexPath.row]
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)

    }
    
    //MARK - Sections
    func setupSections () {
        sectionOne = [ "Impressum", "PrivacyPolicy" ]
        sectionTwo = [ "Rreth nesh", "Kontakt" ]
        sectionThree = [ "Version 1.0.0" ]
    }
}
