//
//  NotificationsViewController.swift
//  Arbresh
//
//  Created by Armend B on 16.01.18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var tableView: UITableView!
    
    private var footerView: UIView!
    private var saveButton: BBRoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor
        
        self.title = "Njoftimet"
        
        self.setupControls()
    }
    
    func setupControls() {
        
        // TableView
        tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        
        // FooterView
        footerView = UIView(frame: CGRect.zero)
        footerView.backgroundColor = UIColor.clear
        view.addSubview(footerView)
        
        // Save Buttom
        saveButton = BBRoundedButton(frame: CGRect.zero)
        saveButton.setTitle("Ruaj", for: UIControlState.normal)
        footerView.addSubview(saveButton)
    }
    
    
    override func viewWillLayoutSubviews() {
        
        var bottomOffset: CGFloat = 0.0
        if #available(iOS 11, *) {
            bottomOffset = view.safeAreaInsets.bottom
        }
        
        // FooterView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.size.height = 60.0 + bottomOffset
            rect.origin.y = self.view.frame.height - rect.size.height
            rect.size.width = self.view.frame.width
            
            footerView.frame = rect
        }
        
        // Save Button
        do {
            var rect = CGRect.zero
            rect.origin.x = 20.0
            rect.size.height = 40
            rect.origin.y = 10
            rect.size.width = footerView.frame.width - 2.0 * rect.origin.x
            
            saveButton.frame = rect
        }
        
        // TableView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.origin.y = 0.0
            rect.size.width = self.view.frame.width
            rect.size.height = self.view.frame.height - footerView.frame.size.height
            
            tableView.frame = rect
        }
    }
    
    //MARK - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        cell?.textLabel?.text = "test"
        cell?.backgroundColor = UIColor.clear
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
    }
}
