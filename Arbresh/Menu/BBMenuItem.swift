//
//  BBMenuItem.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/15/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBMenuItem: NSObject {
    
    private(set) var icon: String!
    private(set) var title: String!
    
    init(icon: String!, title: String!) {
    
        self.icon = icon
        self.title = title
        
        super.init()
    }
}
