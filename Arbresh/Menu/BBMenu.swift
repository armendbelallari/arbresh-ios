//
//  BBMenu.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/15/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

protocol BBMenuDelegate {
    func didSelectMenuItem(withTitle title:String, index:Int)
}

private var menuHeaderView: BBMenuHeaderView!
private var menuFooterView: BBMenuFooterView!

class BBMenu: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var backgroundView:UIButton!
    var menuTable:UITableView!
    var animator:UIDynamicAnimator!
    var menuWidth:CGFloat = 0
    var menuItemIcon = [String]()
    var menuItemTitle = [String]()
    
    var parentViewController = UIViewController()
    
    var menuDelegate:BBMenuDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(menuWidth:CGFloat, items:Array<BBMenuItem>, parentViewController:UIViewController) {
        super.init(frame: CGRect(x: -menuWidth, y: 0, width: menuWidth, height: parentViewController.view.frame.height))

        self.menuWidth = menuWidth
        for item:BBMenuItem in items {
            self.menuItemIcon.append(item.icon)
            self.menuItemTitle.append(item.title)
        }
        
        self.parentViewController = parentViewController
        
        self.backgroundColor = UIColor.backgroundColor
        parentViewController.view.addSubview(self)
        
        setupMenuview()
        
        animator = UIDynamicAnimator(referenceView: parentViewController.view)
        let showMenuRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BBMenu.handleGestures(recognizer:)))
        
        showMenuRecognizer.direction = .right
        parentViewController.view.addGestureRecognizer(showMenuRecognizer)
        
        let hideMenuRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BBMenu.handleGestures(recognizer:)))
        
        hideMenuRecognizer.direction = .left
        parentViewController.view.addGestureRecognizer(showMenuRecognizer)
        
        
    }
    
    @objc func handleGestures(recognizer:UISwipeGestureRecognizer) {
        if recognizer.direction == .right {
            toggleMenu(open: true)
        } else {
            toggleMenu(open: false)
        }
    }
    
    @objc public func toggleMenu(open:Bool) {
        animator.removeAllBehaviors()
        
        let gravityX:CGFloat = open ? 10 : -10
        let pushMagnitude:CGFloat = open ? 2: -20
        let boundaryX:CGFloat = open ? menuWidth : -menuWidth - 5
        
        let gravity = UIGravityBehavior(items: [self])
        gravity.gravityDirection = CGVector(dx: gravityX, dy: 0)
        animator.addBehavior(gravity)
        
        let collision = UICollisionBehavior(items: [self])
        collision.addBoundary(withIdentifier: 1 as NSCopying, from: CGPoint(x: boundaryX, y: 20), to: CGPoint(x: boundaryX, y: parentViewController.view.bounds.height))
        animator.addBehavior(collision)
        
        let push = UIPushBehavior(items: [self], mode: .instantaneous)
        push.magnitude = pushMagnitude
        animator.addBehavior(push)
        
        
        let menuBehavior = UIDynamicItemBehavior(items: [self])
        menuBehavior.elasticity = 0.1
        animator.addBehavior(menuBehavior)
        
        UIView.animate(withDuration: 0.2) {
            self.backgroundView.alpha = open ? 0.5 : 0
        }
    }
    
    func setupMenuview() {
        backgroundView = UIButton(frame: parentViewController.view.bounds)
        backgroundView.addTarget(self, action: #selector(toggleMenu), for: UIControlEvents.touchUpInside)
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0
        parentViewController.view.insertSubview(backgroundView, belowSubview: self)
        
        menuHeaderView = BBMenuHeaderView()
        addSubview(menuHeaderView)
        
        menuFooterView = BBMenuFooterView()
        addSubview(menuFooterView)
        
        menuTable = UITableView(frame: CGRect.zero, style: .plain)
        menuTable.backgroundColor = UIColor.clear
        menuTable.separatorStyle = .none
        menuTable.isScrollEnabled = false
        menuTable.alpha = 1
        
        menuTable.delegate = self
        menuTable.dataSource = self
        menuTable.reloadData()
        addSubview(menuTable)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:BBMenuItemCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? BBMenuItemCell
        
        if cell == nil {
            cell = BBMenuItemCell(style: .default, reuseIdentifier: "Cell")
        }
        
        cell?.titleLabel.text = menuItemTitle[indexPath.row]
        cell?.backgroundColor = UIColor.clear
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
        if let delegate = menuDelegate {
            delegate.didSelectMenuItem(withTitle: menuItemTitle[indexPath.row], index: indexPath.row)
        }
    }
    
    override func layoutSubviews() {
        
        var topOffset: CGFloat = 0.0
        var bottomOffset: CGFloat = 0.0
        
        if #available(iOS 11, *) {
            topOffset = self.safeAreaInsets.top - 5
            bottomOffset = self.safeAreaInsets.bottom
        }
        
        // HeaderView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.origin.y = 0.0
            rect.size.height = 50.0 + topOffset
            rect.size.width = menuWidth
            
            menuHeaderView.frame = rect
        }
        
        // FooterView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.size.height = 50.0 + bottomOffset
            rect.origin.y = self.frame.height - rect.size.height
            rect.size.width = menuWidth
            
            menuFooterView.frame = rect
        }
        
        // TableView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.origin.y = menuHeaderView.frame.maxY
            rect.size.height = self.frame.height - rect.origin.y - menuFooterView.frame.height
            rect.size.width = menuWidth
            
            menuTable.frame = rect
        }
        
    }
}
