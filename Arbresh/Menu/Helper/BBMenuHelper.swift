//
//  BBMenuHelper.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/15/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBMenuHelper: NSObject {
    
    func items() -> Array<BBMenuItem> {
        return itemsInSectionOne
    }
    
    private var itemsInSectionOne: Array<BBMenuItem> {
        
        let home = BBMenuItem.init(icon: "MenuIcon", title: "Home")
        let settings = BBMenuItem.init(icon: "MenuIcon", title: "Settings")
        
        return [ home, settings ]
    }
}
