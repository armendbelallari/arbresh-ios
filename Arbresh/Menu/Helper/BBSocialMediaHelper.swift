//
//  BBSocialMediaHelper.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/16/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBSocialMediaHelper: NSObject {

    func items() -> Array<BBSocialMediaItem> {
        return socialMediaItems
    }
    
    private var socialMediaItems: Array<BBSocialMediaItem> {
        
        let facebook = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.Facebook, mediaUrl: "https://www.facebook.com/arbresh.info/")
        let twitter = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.Twitter, mediaUrl: "https://twitter.com/arbresh_info")
        let instagram = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.Instagram, mediaUrl: "https://www.instagram.com/arbresh.info/")
        let vimeo = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.Vimeo, mediaUrl: "https://vimeo.com/arbresh")
        let googlePlus = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.GooglePlus, mediaUrl: "https://plus.google.com/+ArbreshInfoportali")
        let linkedIn = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.LinkedIn, mediaUrl: "https://www.linkedin.com/company/arbresh-info/")
        let youtube = BBSocialMediaItem.init(frame:CGRect.zero, type: BBSocialMediaType.Youtube, mediaUrl: "https://www.youtube.com/channel/UC8JvF57IAB_GA-MMMu9cnug")
        
        return [ facebook, twitter, instagram, vimeo, googlePlus, linkedIn, youtube ]
    }
}
