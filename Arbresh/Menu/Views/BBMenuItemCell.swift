//
//  MenuItem.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/15/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBMenuItemCell: UITableViewCell {

    class var identifier: String { return String.className(self) }
    
    var iconImageView: UIImageView!
    var titleLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        iconImageView = UIImageView()
        iconImageView.contentMode = .scaleAspectFit
        addSubview(iconImageView)
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.bb_appFontThin(withSize: 17)
        titleLabel.textColor = UIColor.black
        addSubview(titleLabel)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        
        let margin: CGFloat = 6
        let imageSize: CGFloat = 40
        let height: CGFloat = self.frame.height
        
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: margin).isActive = true
        iconImageView.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: imageSize).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: imageSize).isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: margin).isActive = true
        titleLabel.topAnchor.constraint(equalTo: iconImageView.topAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        separatorInset = UIEdgeInsets(top: 0, left: (margin * 2) + imageSize, bottom: 0, right: 0)
    }
    
    open class func height() -> CGFloat {
        return 48
    }
}
