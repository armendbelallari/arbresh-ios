//
//  BBMenuHeaderView.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/16/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

protocol BBMenuHeaderViewProtocol : class {
    func settingsButtonPressed()
}

class BBMenuHeaderView: UIView {

    weak var delegate: BBMenuHeaderViewProtocol?
    
    private var headerImageView: UIImageView!
    private var refreshButton: UIButton!
    private var settingsButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.appColor
        setupControls()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupControls() {
        
        headerImageView = UIImageView()
        headerImageView.image = UIImage(named: "logo-with-icon")
        headerImageView.backgroundColor = UIColor.clear
        headerImageView.contentMode = UIViewContentMode.scaleAspectFit
        addSubview(headerImageView)
        
        refreshButton = UIButton()
        refreshButton.setImage(UIImage(named: "refresh"), for: UIControlState.normal)
        refreshButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        refreshButton.imageEdgeInsets = UIEdgeInsets.init(top: 12, left: 12, bottom: 12, right: 12)
        addSubview(refreshButton)
        
        settingsButton = UIButton()
        settingsButton.setImage(UIImage(named: "settings"), for: UIControlState.normal)
        settingsButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        settingsButton.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        settingsButton.addTarget(self, action: #selector(settingsTapped), for: UIControlEvents.touchUpInside)
        addSubview(settingsButton)
    }
    
    override func layoutSubviews() {
        
        // Settings Button
        do {
            var rect = CGRect.zero
            rect.size.height = 50
            rect.size.width = 50
            rect.origin.x = self.frame.width - rect.size.width - 10
            rect.origin.y = self.frame.height - rect.size.height
            
            settingsButton.frame = rect
        }
        
        // Refresh Button
        do {
            var rect = CGRect.zero
            rect.size.height = 50
            rect.size.width = 50
            rect.origin.x = self.frame.width - rect.size.width * 2 - 10
            rect.origin.y = self.frame.height - rect.size.height
            
            refreshButton.frame = rect
        }
        
        // ImageView
        do {
            var rect = CGRect.zero
            rect.origin.x = 20
            rect.size.height = 40
            rect.size.width = self.frame.width / 2
            rect.origin.y = self.frame.height - rect.size.height - 5
            
            headerImageView.frame = rect
        }
    }
    
    @objc func settingsTapped() {
        delegate?.settingsButtonPressed()
    }
}
