//
//  BBMenuFooterView.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/16/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBMenuFooterView: UIView {

    var socialMediaItems: Array<BBSocialMediaItem>!
    var separatorView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let socialMediaHelper = BBSocialMediaHelper()
        socialMediaItems = socialMediaHelper.items()
        
        setupControls()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupControls() {
        
        separatorView = UIView()
        separatorView.backgroundColor = UIColor.appColor
        addSubview(separatorView)
        
        for item: BBSocialMediaItem in socialMediaItems {
            addSubview(item)
        }
    }
    
    override func layoutSubviews() {

        // Separator View
        do {
            var rect = CGRect.zero
            rect.origin.x = 10
            rect.origin.y = 0
            rect.size.width = self.frame.width - rect.origin.x * 2.0
            rect.size.height = 0.5
            
            separatorView.frame = rect
        }
        
        
        // Buttons 
        var itemXPossition: CGFloat = 5
        for item: BBSocialMediaItem in socialMediaItems {
            var rect = CGRect.zero
            rect.origin.x = itemXPossition
            rect.size.height = 37.0
            rect.size.width = 37.0
            rect.origin.y = 5.0
            item.frame = rect;
            
            itemXPossition += rect.size.width + 5
        }
    }
}
