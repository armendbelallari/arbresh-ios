//
//  BBSocialMediaItem.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/16/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

enum BBSocialMediaType {
    case Facebook
    case Twitter
    case Instagram
    case Vimeo
    case GooglePlus
    case LinkedIn
    case Youtube
}

class BBSocialMediaItem: UIButton {

    var icon: UIImage!
    var title: String!
    var url: String!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, type: BBSocialMediaType, mediaUrl: String!) {
        super.init(frame: frame)
        
        self.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.backgroundColor = UIColor.clear
        self.imageEdgeInsets = UIEdgeInsets.init(top: 7.5, left: 7.5, bottom: 7.5, right: 7.5)
        
        
        url = mediaUrl
        updateMediaType(mediaType: type)
    }
    
    func updateMediaType (mediaType: BBSocialMediaType) {
        switch mediaType {
        case .Facebook:
            icon = #imageLiteral(resourceName: "facebook-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "Facebook"
        case .Twitter:
            icon = #imageLiteral(resourceName: "twitter-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "Twitter"
        case .Instagram:
            icon = #imageLiteral(resourceName: "instagram-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "Instagram"
        case .Vimeo:
            icon = #imageLiteral(resourceName: "vimeo-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "Vimeo"
        case .GooglePlus:
            icon = #imageLiteral(resourceName: "google-plus-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "GooglePlus"
        case .LinkedIn:
            icon = #imageLiteral(resourceName: "linkedin-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "LinkedIn"
        case .Youtube:
            icon = #imageLiteral(resourceName: "youtube-logo")
            self.setImage(icon, for: UIControlState.normal)
            title = "Youtube"
        }
    }
}
