//
//  LeftMenuViewController.swift
//  Arbresh
//
//  Created by Armend Belallari on 21.01.18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case main = 0
    case settings
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

private var menuHeaderView: BBMenuHeaderView!
private var menuFooterView: BBMenuFooterView!

class LeftMenuViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, LeftMenuProtocol, BBMenuHeaderViewProtocol {
    
    var menuTable:UITableView!
    
    var mainViewController: UIViewController!
    var settingsViewController: UIViewController!
    
    var menuItemIcon = [String]()
    var menuItemTitle = [String]()
    
    var navController = UINavigationController()
    
    func setMenuItems(items:Array<BBMenuItem>) {
        for item:BBMenuItem in items {
            self.menuItemIcon.append(item.icon)
            self.menuItemTitle.append(item.title)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.backgroundColor
        
        setMenuItems(items: menuItems())
        
        settingsViewController = SettingsViewController()
        self.settingsViewController = UINavigationController(rootViewController: settingsViewController)
        
        setupMenuview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .settings:
            navController.pushViewController(SettingsViewController(), animated: true)
            closeLeft()
        }
    }
    
    func setupMenuview() {
        menuHeaderView = BBMenuHeaderView()
        menuHeaderView.delegate = self
        view.addSubview(menuHeaderView)
        
        menuFooterView = BBMenuFooterView()
        view.addSubview(menuFooterView)
        
        menuTable = UITableView(frame: CGRect.zero, style: .plain)
        menuTable.backgroundColor = UIColor.clear
        menuTable.separatorStyle = .none
        menuTable.isScrollEnabled = false
        menuTable.alpha = 1
        
        menuTable.delegate = self
        menuTable.dataSource = self
        menuTable.reloadData()
        view.addSubview(menuTable)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:BBMenuItemCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? BBMenuItemCell
        
        if cell == nil {
            cell = BBMenuItemCell(style: .default, reuseIdentifier: "Cell")
        }
        
        cell?.titleLabel.text = menuItemTitle[indexPath.row]
        cell?.backgroundColor = UIColor.clear
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
    }
    
    override func viewWillLayoutSubviews() {
        
        var topOffset: CGFloat = 0.0
        var bottomOffset: CGFloat = 0.0
        
        if #available(iOS 11, *) {
            topOffset = self.view.safeAreaInsets.top - 5
            bottomOffset = self.view.safeAreaInsets.bottom
        }
        
        // HeaderView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.origin.y = 0.0
            rect.size.height = 50.0 + topOffset
            rect.size.width = self.view.frame.width
            
            menuHeaderView.frame = rect
        }
        
        // FooterView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.size.height = 50.0 + bottomOffset
            rect.origin.y = self.view.frame.height - rect.size.height
            rect.size.width = self.view.frame.width
            
            menuFooterView.frame = rect
        }
        
        // TableView
        do {
            var rect = CGRect.zero
            rect.origin.x = 0.0
            rect.origin.y = menuHeaderView.frame.maxY
            rect.size.height = self.view.frame.height - rect.origin.y - menuFooterView.frame.height
            rect.size.width = self.view.frame.width
            
            menuTable.frame = rect
        }
        
    }
    
    //MARK - Temp Menu Items
    func menuItems() -> Array<BBMenuItem> {
        let main = BBMenuItem(icon: "menu", title: "Main")
        let settings = BBMenuItem(icon: "menu", title: "Settings")
        
        var menuItems = Array<BBMenuItem>()
        menuItems.append(main)
        menuItems.append(settings)
        
        return menuItems
    }
    
    func settingsButtonPressed() {
        changeViewController(.settings)
    }
    
}

