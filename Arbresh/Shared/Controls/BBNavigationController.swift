//
//  BBNavigationController.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/14/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBRNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navigationBarAppearace = UINavigationBar.appearance()
        
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.barTintColor = UIColor.appColor
        navigationBarAppearace.isTranslucent = false
        
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }
}
