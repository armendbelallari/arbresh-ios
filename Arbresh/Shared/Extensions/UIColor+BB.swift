//
//  UIColor+BB.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/14/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

extension UIColor {
    
    //MARK: View
    @nonobjc static var backgroundColor: UIColor  { return UIColor(hexString: "edecec") }
    @nonobjc static var appColor: UIColor  { return UIColor(hexString: "3a469a") }
}
