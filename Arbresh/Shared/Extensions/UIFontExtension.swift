//
//  UIFontExtension.swift
//  Arbresh
//
//  Created by Armend Belallari on 1/14/18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

extension UIFont {
    class func bb_appFontThin(withSize size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Thin", size: size)!
    }
}
