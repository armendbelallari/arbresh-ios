//
//  BBRoundedButton.swift
//  Arbresh
//
//  Created by Armend B on 16.01.18.
//  Copyright © 2018 BlueBerry. All rights reserved.
//

import UIKit

class BBRoundedButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.appColor
        clipsToBounds = true
        layer.cornerRadius = 5.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
